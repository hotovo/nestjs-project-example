# REST example project with Nestjs

In order to run it:
- setup Postgres DB as per `ormconfig.json` config file  
- `npm i` to install dependencies
- `npm run start:dev` to run in dev mode
- access at `localhost:3000/api/v1/vaccination-records` and related endpoints

# Swagger
Swagger API documentation is available at `localhost:3000/api/v1/docs` to see available endpoints

# Technology
Nest js is mature framework for building fully capable REST server solutions. Support annotations for quick and efficient development.
Provides out of the box all tools as routing, validations, error handling and support for plugins to extend the functionality.
As an example of such extension serves plugin to provide Swagger documentation of the endpoints with minimal configuration. 

TypeORM is ORM framework supporting multiple databases with seamless integration with Nestjs.

CRUD extension for Nestjs allows to reduce boilerplate code for standard REST CRUD operations and provide seamless integration with Nestjs and TypeORM.

# Testing
Jest testing framework is used form unit testing parts of the server implementation
export const buildCsv = <T>(keys: string[], values: T[]) => {
  // naive implementation
  let csv = '';
  const headers = keys;

  csv += `"${headers.join('","')}"\n`;

  values.forEach((person, index, array) => {
    const row = headers
      .map((key) => {
        const value = person[key];

        if (typeof value === 'string') {
          return `"${value || ''}"`;
        } else if (value instanceof Date) {
          return `"${value.toISOString() || ''}"`;
        }
        return `${value || ''}`;
      })
      .join(',');
    csv += row;

    if (index < array.length - 1) {
      csv += '\n';
    }
  });

  return csv;
};

import { Logger, Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { RecordsService } from './records.service';
import { RecordsController } from './records.controller';
import { Record } from './record.entity';
import { SeedingService } from 'src/records/seeding/seeding.service';

@Module({
  imports: [TypeOrmModule.forFeature([Record])],
  providers: [RecordsService, SeedingService, Logger],
  controllers: [RecordsController],
})
export class RecordsModule {}

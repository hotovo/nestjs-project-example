import { Controller, Get, Headers, Res } from '@nestjs/common';
import { Crud, CrudController } from '@nestjsx/crud';
import { Response } from 'express';

import { buildCsv } from '../utils/helpers';
import { CreateRecordDto } from './create-record.dto';
import { Record, RecordKeys } from './record.entity';
import { RecordsService } from './records.service';
import { ReplacePersonDto } from './replace-person.dto';
import { UpdatePersonDto } from './update-person.dto';

@Crud({
  model: {
    type: Record,
  },
  params: {
    uuid: {
      field: 'uuid',
      type: 'uuid',
      primary: true,
    },
  },
  query: {
    maxLimit: 100,
    limit: 50,
    alwaysPaginate: true,
  },
  dto: {
    create: CreateRecordDto,
    update: UpdatePersonDto,
    replace: ReplacePersonDto,
  },
})
@Controller('vaccination-records')
export class RecordsController implements CrudController<Record> {
  constructor(public service: RecordsService) {}

  @Get('export')
  async exportCsv(@Headers('Accept') accept, @Res() response: Response) {
    const results = await this.service.find();

    if (accept === 'text/csv') {
      response.set('Content-Type', 'text/csv; charset=utf-8');
      response.send(buildCsv(RecordKeys, results));
    } else {
      return results;
    }
  }
}

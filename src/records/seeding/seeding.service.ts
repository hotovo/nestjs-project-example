import {
  Injectable,
  OnApplicationBootstrap,
  Logger,
  Inject,
  LoggerService,
} from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { Repository } from 'typeorm';
import * as csvParser from 'csv-parser';
import { createReadStream } from 'fs';

import { Record } from 'src/records/record.entity';

@Injectable()
export class SeedingService implements OnApplicationBootstrap {
  private readonly name: 'SeedingService';
  constructor(
    @InjectRepository(Record)
    private readonly repository: Repository<Record>,
    @Inject(Logger)
    private readonly logger: LoggerService,
  ) {}

  parseCsvData() {
    const parsedData = [];

    return new Promise((resolve, reject) =>
      createReadStream('./data/vaccination.csv')
        .pipe(
          csvParser({
            mapValues: ({ header, value }) => {
              const key = header as keyof Record;

              switch (key) {
                case 'age':
                  return parseInt(value);

                case 'vaccinationDate':
                  return value ? new Date(value) : undefined;

                case 'updatedAt':
                  return new Date(value);

                default:
                  return value;
              }
            },
          }),
        )
        .on('data', (data) => {
          parsedData.push(data);
        })
        .on('end', () => {
          resolve(parsedData);
        })
        .on('error', (err) => {
          this.logger.error(
            `Data seeding csv parsing error - ${err.name}: ${err.message}`,
            this.name,
          );
          reject(err);
        }),
    );
  }

  async onApplicationBootstrap() {
    this.logger.log('Data seeding started', this.name);

    const records = await this.parseCsvData();

    try {
      await this.repository.save(records, { chunk: 1000 });
    } catch (err) {
      this.logger.error(
        `Data seeding db import error - ${err.name}: ${err.message}`,
        this.name,
      );
    }

    this.logger.log('Data seeding done', this.name);

    return;
  }
}

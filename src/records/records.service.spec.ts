import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { MockRepository } from '../mocks/repository.mock';
import { Record } from './record.entity';
import { RecordsService } from './records.service';

describe('RecordsService', () => {
  let personsService: RecordsService;
  let repositoryMock: MockRepository<Record>;

  beforeEach(async () => {
    repositoryMock = new MockRepository();

    const app: TestingModule = await Test.createTestingModule({
      providers: [
        RecordsService,
        {
          provide: getRepositoryToken(Record),
          useValue: repositoryMock,
        },
      ],
    }).compile();

    personsService = app.get<RecordsService>(RecordsService);
  });

  describe('person service', () => {
    beforeEach(() => {
      repositoryMock.find.mockReturnValue([]);
    });

    it('should call find on repository', () => {
      expect(personsService.find()).toHaveLength(0);
      expect(repositoryMock.find).toHaveBeenCalled();
    });
  });
});

import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { TypeOrmCrudService } from '@nestjsx/crud-typeorm';
import { Repository } from 'typeorm';

import { Record } from './record.entity';

@Injectable()
export class RecordsService extends TypeOrmCrudService<Record> {
  constructor(@InjectRepository(Record) repository: Repository<Record>) {
    super(repository);
  }
}

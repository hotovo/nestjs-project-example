import { Test, TestingModule } from '@nestjs/testing';
import { getRepositoryToken } from '@nestjs/typeorm';
import { MockRepository } from '../mocks/repository.mock';
import { Record } from './record.entity';
import { RecordsController } from './records.controller';
import { RecordsService } from './records.service';

describe('RecordsController', () => {
  let personsController: RecordsController;
  let repositoryMock: MockRepository<Record>;

  beforeEach(async () => {
    repositoryMock = new MockRepository();

    const app: TestingModule = await Test.createTestingModule({
      providers: [
        RecordsController,
        RecordsService,
        {
          provide: getRepositoryToken(Record),
          useValue: repositoryMock,
        },
      ],
    }).compile();

    personsController = app.get(RecordsController);
  });

  describe('person service', () => {
    let responseMock: { send: jest.Mock; set: jest.Mock };

    beforeEach(() => {
      responseMock = {
        set: jest.fn(),
        send: jest.fn(),
      };
    });

    afterEach(() => {
      responseMock = undefined;
    });

    it('should send empty json array by default', async () => {
      repositoryMock.find.mockReturnValue([]);

      const response = await personsController.exportCsv(
        '',
        responseMock as any,
      );

      expect(repositoryMock.find).toHaveBeenCalled();
      expect(response).toStrictEqual([]);
    });

    it('should build empty csv when accept header is set', async () => {
      repositoryMock.find.mockReturnValue([]);

      responseMock.send.mockImplementation((value) => {
        expect(value).toBe(
          '"uuid","firstName","lastName","age","vaccinationDate","vaccinationState","vaccine","updatedAt"\n',
        );
      });

      await personsController.exportCsv('tex/csv', responseMock as any);

      expect(repositoryMock.find).toHaveBeenCalled();
    });

    it('should build csv with two rows for 2 results', async () => {
      repositoryMock.find.mockReturnValue([{ uuid: '1' }, { uuid: '2' }]);

      let sentResponse: string;
      responseMock.send.mockImplementation((value) => {
        sentResponse = value;
      });

      await personsController.exportCsv('text/csv', responseMock as any);

      expect(repositoryMock.find).toHaveBeenCalled();
      expect(sentResponse.split('\n')).toHaveLength(3);
    });
  });
});

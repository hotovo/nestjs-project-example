import { PartialType } from '@nestjs/swagger';
import { BaseRecordDto } from './base-record.dto';

export class UpdatePersonDto extends PartialType(BaseRecordDto) {}

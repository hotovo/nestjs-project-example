import { ApiProperty } from '@nestjs/swagger';
import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
} from 'typeorm';
import { RecordInterface } from './record.interface';
import { VaccinationState, Vaccine } from './records.enums';

@Entity()
export class Record implements RecordInterface {
  @ApiProperty({})
  @PrimaryGeneratedColumn('uuid')
  uuid: string;

  @ApiProperty()
  @Column()
  firstName: string;

  @ApiProperty()
  @Column()
  lastName: string;

  @ApiProperty()
  @Column('int')
  age: number;

  @ApiProperty({
    enum: VaccinationState,
  })
  @Column({
    type: 'enum',
    enum: VaccinationState,
    default: VaccinationState.NONE,
  })
  vaccinationState: VaccinationState;

  @ApiProperty({
    enum: Vaccine,
  })
  @Column({
    type: 'enum',
    enum: Vaccine,
    default: Vaccine.NONE,
  })
  vaccine: Vaccine;

  @ApiProperty()
  @Column({
    type: 'timestamp',
    nullable: true,
    default: null,
  })
  vaccinationDate: Date | null;

  @ApiProperty()
  @UpdateDateColumn({ type: 'timestamp' })
  updatedAt: Date;
}

export const RecordKeys: (keyof Record)[] = [
  'uuid',
  'firstName',
  'lastName',
  'age',
  'vaccinationDate',
  'vaccinationState',
  'vaccine',
  'updatedAt',
];

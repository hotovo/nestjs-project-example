import { VaccinationState, Vaccine } from './records.enums';

export interface RecordInterface {
  firstName: string;
  lastName: string;
  age: number;
  vaccinationState: VaccinationState;

  vaccine: Vaccine;

  vaccinationDate: Date | null;
}

import { ApiProperty } from '@nestjs/swagger';
import { Type } from 'class-transformer';
import {
  IsDate,
  IsEnum,
  IsNotEmpty,
  IsNumber,
  IsOptional,
  IsString,
  MaxLength,
  Min,
  MinLength,
} from 'class-validator';
import { RecordInterface } from './record.interface';
import {
  VaccinationState,
  VaccinationStateValues,
  Vaccine,
  VaccineValues,
} from './records.enums';

export class BaseRecordDto implements RecordInterface {
  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @MaxLength(100)
  firstName: string;

  @ApiProperty()
  @IsNotEmpty()
  @IsString()
  @MinLength(2)
  @MaxLength(100)
  lastName: string;

  @ApiProperty()
  @IsNumber({ allowInfinity: false })
  @Min(18)
  age: number;

  @ApiProperty({
    enum: VaccinationState,
  })
  @IsEnum(VaccinationState, {
    message: `Vaccination state have to be one of [${VaccinationStateValues}]`,
  })
  vaccinationState: VaccinationState;

  @ApiProperty({
    enum: Vaccine,
  })
  @IsEnum(Vaccine, {
    message: `Vaccine value have to be one of [${VaccineValues}]`,
  })
  vaccine: Vaccine;

  @ApiProperty()
  @Type(() => Date)
  @IsDate()
  @IsOptional()
  vaccinationDate: Date | null;
}

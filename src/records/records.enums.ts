export enum VaccinationState {
  NONE = 'none',
  INITIAL = 'initial',
  FULL = 'full',
}

export const VaccinationStateValues = Object.values(VaccinationState);
export enum Vaccine {
  PFIZER_BIONTEC = 'pfizer-biontec',
  ASTRA_ZENECA = 'astra-zeneca',
  JOHNSON = 'johnson',
  NONE = 'none',
}

export const VaccineValues = Object.values(Vaccine);
